#include <stdio.h>
#include "stack.h"
#include <stdlib.h>

#define ERR_TEXT_RED "\x1b[31m"
#define TEXT_COLOR_RESET "\x1b[0m"

stackT *NewStack(void)
{
	stackT *newStack = (stackT *) malloc(sizeof(stackT));
	newStack->head = NULL;
	return newStack;

}

void Push(stackT *stack, valueT value)
{
	nodeT *newNode =(nodeT*) malloc(sizeof(nodeT));
	newNode->value = value;
	newNode->next = stack->head;
	stack->head = newNode;
	newNode = NULL;	
}

valueT Pop(stackT *stack)
{
	if(stack->head == NULL)
	{	
		printf(ERR_TEXT_RED "The Stack is EMPTY! Nothing to Pop!" TEXT_COLOR_RESET"\n");
		return 0;
	}
	else
	{
		nodeT *temp;
		temp = stack->head;
		char popVal = temp->value;
		stack->head = temp->next;
		free(temp);
		return popVal;
	}	
}

void EmptyStack(stackT *stack)
{
	while(stack->head != NULL)
	{
		nodeT *temp;
        	temp = stack->head;
        	stack->head = temp->next;
        	free(temp);
	}
}

void FreeStack(stackT *stack)
{
	if(stack->head == NULL)
	{
		free(stack);		
	}
	else
	{
		printf(ERR_TEXT_RED "Stack is NOT EMPTY! Cannot free stack!" TEXT_COLOR_RESET"\n");

	}
}

bool IsEmpty(stackT *stack)
{
	bool b;
	if(stack->head == NULL)
	{
		b = true;
	}
	else
	{
		b = false;
	}
	return b;
}
